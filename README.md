# GearSystem

GearSystem is a SEGA Master System/Gear Gear emulator for Ubuntu Touch. This project is a fork of Ryan Pattison's GearSystem port for Ubuntu Touch which is a fork of Ignacio Sanchez's GearSystem, focusing on Qt/QML and OpenGL ES for mobile phones.

<img src="platforms/ubuntu_touch/gearsystem.png" width="64">
<img src="platforms/ubuntu_touch/screenshots/landscape.png" width="256">
<img src="platforms/ubuntu_touch/screenshots/portrait.png" height="256">

[![OpenStore](https://open-store.io/badges/en_US.png)](https://open-store.io/app/gearsystem.martinferretti)

## Build Instructions

### Ubuntu Touch

- [Install Clickable](http://clickable.bhdouglass.com/en/latest/)
- `clickable -c platforms/ubuntu_touch/clickable.json`
- Clickable will compile the project and install it on your connected device
